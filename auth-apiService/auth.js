import jwt from 'jsonwebtoken';
import Tokens from 'csrf';
import moment from 'moment';

// Other services
import UserService from './user';
import EmailSender from './emailSender';

// Data access
import { MasterRepository } from './../repos';

// helper utils
import {
  pbkdf2,
  randomString,
  log,
} from '../utils';

import {
  StringUtils,
} from '../../common/utils';

// Configuration
import { settings } from './../config';

/**
 * Creates an AuthService singleton.
 * @class
 */
class AuthService {

  /**
   * AuthService.authenticate
   *
   * Attempt to authenticate as a given user.
   * @param passwordInput The password sent to be verified.
   * @param user The user object to compare for authentication.
   * @returns {User} [authUser]
   */
  static authenticate(passwordInput, user) {
    const {
      password,
      salt,
    } = user;

    const hashedInput = pbkdf2(passwordInput, salt);

    if (StringUtils.compare(password, hashedInput)) {
      return user;
    }

    return null;
  }

  /**
   * AuthService.createSessionToken
   *
   * Create a jsonwebtoken to be used for bearer authorization.
   *
   * @param userData The user data to encode.
   * @returns {*} The encoded token.
   */
  static createSessionToken(userData) {
    let encoded;

    try {
      encoded = jwt.sign({
        iss: 'medean.com',
        exp: Math.floor(Date.now() / 1000) + (60 * 60), // exp in 1 hour (uses seconds)
        data: userData,
      }, process.env.SESSION_KEY);
    } catch (err) {
      throw err;
    }

    return encoded;
  }

  /**
   * AuthService.verifySessionToken
   *
   * Try to decode the given jsonwebtoken and return the result.
   * @param sessionToken The token to decode.
   * @returns {*} The decoded result.
   */
  static verifySessionToken(sessionToken) {
    let decoded;

    try {
      decoded = jwt.verify(sessionToken, process.env.SESSION_KEY);
    } catch (err) {
      throw err;
    }

    return decoded;
  }

  /**
   * AuthController.createXsrfToken
   *
   * Create an anti-xsrf token to encode in the JWT and attach to an HttpOnly cookie.
   */
  static createXsrfToken() {
    const tokens = new Tokens();
    const secret = tokens.secretSync();
    const xsrfToken = tokens.create(secret);

    return xsrfToken;
  }

  /**
   * Processes a request to send the forgot password email
   * 1. Check for the existence of user
   * 2. If no user, sends the no user email
   * 3. If user exists, creates a token, stores it, emails a reset URL
   * @param email
   * @returns {Promise.<void>}
   */
  static async processForgotPasswordRequest(email) {
    if (!email) throw new Error('Missing parameter email');

    // Get the user by the Email address
    const user = await UserService.findUser(email);

    // If email does not exist, send notification email
    if (!user) await EmailSender.sendForgotPasswordNoUserEmail(email);
    // If email exists, generate the reset password token and send it to the email
    else {
      // Get the reset token
      const token = await AuthService.getResetPasswordToken(user);
      // Send the reset email
      await EmailSender.sendResetPasswordEmail(user, token);
    }
  }

  /**
   * Gets a new password reset token
   * Also invalidates existing records and saves the new one
   * @param user
   * @returns {Promise.<*>}
   */
  static async getResetPasswordToken(user) {
    // Mark as invalid any existing tokens
    await MasterRepository.invalidateAllPasswordResetByUser(user.id);
    // Generate new token of 64 bytes in length
    const token = randomString(64);
    // Store the new values
    await AuthService.persistResetPasswordToken(user.id, user.email, token);
    // Return to the user
    return token;
  }

  /**
   * Stores the token in the database for later validation
   * @param userId PK of user this token is for
   * @param email email of the user
   * @param token string value of the token, cannot be more than 128 characters
   * @returns {Promise.<void>}
   */
  static async persistResetPasswordToken(userId, email, token) {
    // Make sure we have value arguments
    if (!userId) throw new Error('Missing method parameter userId');
    else if (!email) throw new Error('Missing method parameter email');
    else if (!token) throw new Error('Missing method parameter token');
    else if (token.length !== 128) throw new Error('Token must be 128 characters');

    // Generate a salt value
    const tokenSalt = randomString(512);

    // hash the token
    const tokenHash = pbkdf2(token, tokenSalt);

    // Store the values
    await MasterRepository.insertPasswordReset(userId, tokenHash, tokenSalt);
  }

  /**
   * Given a token and an email address, check if this combination matches an existing record
   * This method will return the matching PasswordReset record and user account if they exist
   * This is useful for marking it as used further down the route chain
   * If there is no matching record, undefined is returned
   * @param token Non-hashed token to use
   * @param email Email of user which is requesting a password reset
   * @returns {Promise.<{User, PasswordReset}||undefined>}
   */
  static async validateResetToken(token, email) {
    // Get the user by the Email address
    const user = await UserService.findUser(email);

    // If the user exists, attempt to valiate the password reset token
    if (user) {
      // Load a valid (not used, not invalidated) record for this user
      const passwordReset = await AuthService.getValidPasswordResetRecord(user.id);
      // Check if it is within our time limit and the hashes match
      const isWithinTimeFrame = await AuthService.isPasswordResetWithinTimeLimit(passwordReset);
      const isMatchingToken = AuthService.isMatchingPasswordResetToken(token, passwordReset);
      if (isWithinTimeFrame && isMatchingToken) {
        // It is a match - thus we return the matching record and the user
        return {
          user,
          passwordReset,
        };
      }
    }
    // The token or the user is not valid
    return undefined;
  }

  /**
   * Given a password reset object and a token, check if the token is for this password reset object
   * E.g. Is the hash of the token the same as the hash stord in the password reset object?
   * @param token Non-hashed token value to compare against the hashed password reset object
   * @param passwordReset The comparison object. Must include tokenHash and tokenSalt properties
   * @returns {boolean} True if they match, false if not
   */
  static isMatchingPasswordResetToken(token, passwordReset) {
    // If the parameters were not provided, they are not a match
    if (!token || !passwordReset || !passwordReset.tokenHash || !passwordReset.tokenSalt) {
      return false;
    }

    // Get the hash of the provided token
    const userProvidedTokenHash = pbkdf2(token, passwordReset.tokenSalt);

    // Check if they match
    return StringUtils.compare(userProvidedTokenHash, passwordReset.tokenHash);
  }

  /**
   * Returns a valid password reset object for the user, assuming only 1 exists
   * A valid one is one which:
   *  - is not used (used === false)
   *  - hasn't been invalidated (invalidated === false)
   *  - has no peers (no other valid records for this user)
   * During generation of reset records, all previous records are invalidated.
   * Given this, there should only ever be 1 active and valid record.
   * If there is more than 1 valid record, no record is returned as the state of more than 1
   * valid record implies an issue has occurred, where an issue could be code related
   * or an actual attack on our password reset system.
   * @param userId
   * @returns {Promise.<PasswordReset>} PasswordReset if exists, undefined if no valid records exist
   */
  static async getValidPasswordResetRecord(userId) {
    // Load from the DB any valid password reset objects for the given user
    const existingTokens = await MasterRepository.getValidPasswordResetByUser(userId);
    // Check if there is only 1 existing valid record
    if (existingTokens && existingTokens.length === 1) {
      // Return the only existing record to the user
      return existingTokens[0];
    }
    // Return undefined as there is not only 1 valid token
    return undefined;
  }

  /**
   * Checks if the passwordReset object is still applicable according to our timeout rules
   * @param passwordReset Object to compare. Must have a createdAt property.
   * @returns {Promise.<boolean>} True if within time frame, false if not.
   */
  static async isPasswordResetWithinTimeLimit(passwordReset) {
    // If no passwordReset object was provided, it is invalid
    if (!passwordReset || !passwordReset.createdAt) return false;
    // This earliest valid creation time, X minutes ago, for X minutes of validity
    const minTime = moment().subtract(settings.passwordResetTimeLimit, 'minutes');
    // The latest valid creation time.
    // We limit it in case a record with a bad date is entered by mistake or during attack.
    const maxTime = moment();
    // When the password reset was generated
    const createdDateTime = moment(passwordReset.createdAt);
    /**
     * Return the result of the comparison
     * Argument 3 is the precision of the comparison
     * Argument 4 specifies we want inclusion comparison, e.g. >=. '()' would be exclusive.
     */
    return createdDateTime.isBetween(minTime, maxTime, 'second', '[]');
  }

  /**
   * Updates the record to be used
   * @param passwordReset
   * @returns {Promise.<Promise|*>}
   */
  static async setPasswordResetAsUsed(passwordReset) {
    return MasterRepository.setPasswordResetAsUsed(passwordReset.id);
  }

  /**
   * Persists the record of a userLogin
   * @param userId The ID of the user to associate this record to
   * @param ip The IP of the login
   * @returns {Promise.<void>}
   */
  static async recordUserLogin(userId, ip) {
    return MasterRepository.insertUserLogin(userId, ip);
  }
}

export default AuthService;
