import axios from 'axios';

export async function requestTips(token, userId) {
  const path = `/api/users/${userId}/tips`;
  const options = {
    headers: {
      Authorization: `bearer ${token}`,
    },
  };

  return axios.get(path, options);
}

export async function requestPopularTips(token) {
  const path = '/api/tips/popular';
  const options = {
    headers: {
      Authorization: `bearer ${token}`,
    },
  };

  return axios.get(path, options);
}

export async function requestVoteTip(token, userId, tipId, vote) {
  const path = `/api/users/${userId}/tips/${tipId}`;
  const tipUpdates = {
    vote,
  };
  const options = {
    headers: {
      Authorization: `bearer ${token}`,
    },
  };

  return axios.patch(path, tipUpdates, options);
}

export async function requestDismissTip(token, userId, tipId) {
  const path = `/api/users/${userId}/tips/${tipId}`;
  const tipUpdates = {
    dismissed: true,
  };
  const options = {
    headers: {
      Authorization: `bearer ${token}`,
    },
  };

  return axios.patch(path, tipUpdates, options);
}
