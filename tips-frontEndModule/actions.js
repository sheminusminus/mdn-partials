import * as constants from './constants';

export function tipsRequest(token, userId) {
  return {
    type: constants.TIPS_REQUEST,
    payload: {
      token,
      userId,
    },
  };
}

export function tipsSuccess(tips) {
  return {
    type: constants.TIPS_SUCCESS,
    payload: {
      tips,
    },
  };
}

export function tipsFailure(error) {
  return {
    type: constants.TIPS_FAILURE,
    payload: error,
  };
}

export function dismissTipRequest(token, userId, tipId) {
  return {
    type: constants.TIP_DISMISS_REQUEST,
    payload: {
      token,
      userId,
      tipId,
    },
  };
}

export function dismissTipSuccess(tipId, dismissed) {
  return {
    type: constants.TIP_DISMISS_SUCCESS,
    payload: {
      tipId,
      dismissed,
    },
  };
}

export function dismissTipFailure(error) {
  return {
    type: constants.TIP_DISMISS_FAILURE,
    error,
  };
}

export function voteTipRequest(token, userId, tipId, vote) {
  return {
    type: constants.TIP_VOTE_REQUEST,
    payload: {
      token,
      userId,
      tipId,
      vote,
    },
  };
}

export function voteTipSuccess(tipId, vote) {
  return {
    type: constants.TIP_VOTE_SUCCESS,
    payload: {
      tipId,
      vote,
    },
  };
}

export function voteTipFailure(error) {
  return {
    type: constants.TIP_VOTE_FAILURE,
    payload: error,
  };
}

export function popularTipsRequest(token, userId) {
  return {
    type: constants.POPULAR_TIPS_REQUEST,
    payload: {
      token,
      userId,
    },
  };
}

export function popularTipsSuccess(tips) {
  return {
    type: constants.POPULAR_TIPS_SUCCESS,
    payload: {
      tips,
    },
  };
}

export function popularTipsFailure(error) {
  return {
    type: constants.POPULAR_TIPS_FAILURE,
    payload: error,
  };
}

export function selectTipsFactorFilter(factorFilter) {
  return {
    type: constants.TIPS_FACTOR_FILTER_SELECTED,
    payload: {
      factorFilter,
    },
  };
}
