import { createSelector } from 'reselect';
import { filter, find, uniqBy, concat } from 'lodash';

import { getTransactionFilterCode } from 'modules/ui/selectors';
import { getScoreFactors } from 'modules/score/selectors';

const getTipData = state => state.tipData;

export const getAllTips = createSelector(
  [getTipData],
  tipData => tipData.tips,
);

export const getPopularTips = createSelector(
  [getTipData],
  tipData => tipData.popular,
);

export const getTipsFactor = createSelector(
  [getTipData],
  tipData => tipData.selectedTipsFactor,
);

/**
 * tipsWithFactorNames
 * Add the factor names to the tips, to display on each tips page item.
 * @param tips All the tips.
 * @param factors All the score factors.
 */
const tipsWithFactorNames = (tips, factors) => (
  tips.map((tip) => {
    const factor = find(factors, f => f.categoryCode === tip.scoreFactorCode);
    const factorName = factor ? factor.name : '';
    return {
      ...tip,
      factorName,
    };
  })
);

/**
 * applicableFactors
 * Get the just factors that have tips associated with them, to display in the tips filter.
 * @param tips All the tips.
 * @param factors All the score factors, to be filtered down.
 * @returns {any[][] | any[]}
 */
const applicableFactors = (tips, factors) => {
  // create an item for displaying all factors (no filter applied)
  const allFactorsItem = { label: 'All Factors', value: 'ALL' };

  // get the factors that have tips, concatenated with the 'all' item
  const visibleFilters = concat([allFactorsItem], tips.map((tip) => {
    const factor = find(factors, f => f.categoryCode === tip.scoreFactorCode);
    return {
      label: factor ? factor.name : '',
      value: factor ? factor.categoryCode : '',
    };
  }));

  // return an array of unique factors
  return uniqBy(visibleFilters, 'value');
};

export const getAllUndismissedTips = createSelector(
  [getAllTips],
  tips => tips.filter(tip => tip.dismissed === false),
);

export const getTipsForTransactionCategory = createSelector(
  [getAllUndismissedTips, getTransactionFilterCode],
  (tips, categoryCode) => (
    filter(tips, tip => tip.scoreFactorCode === categoryCode)
  ),
);

export const getTips = createSelector(
  [getAllTips],
  tips => tips,
);

export const getSelectedTipsFactor = createSelector(
  [getTipsFactor],
  factor => factor,
);

export const getTipsWithFactorNames = createSelector(
  [getAllTips, getScoreFactors],
  (tips, factors) => tipsWithFactorNames(tips, factors),
);

export const getPopularTipsWithFactorNames = createSelector(
  [getPopularTips, getScoreFactors],
  (tips, factors) => tipsWithFactorNames(tips, factors),
);

export const getApplicableFactors = createSelector(
  [getAllTips, getScoreFactors],
  (tips, factors) => applicableFactors(tips, factors),
);
