import { call, fork, put, take } from 'redux-saga/effects';

import * as actions from './actions';
import * as api from './api';

import {
  TIPS_REQUEST,
  TIP_VOTE_REQUEST,
  TIP_DISMISS_REQUEST,
  POPULAR_TIPS_REQUEST,
} from './constants';

import {
  allTips,
  popularTips,
} from './parse';

/**
 *  requestTips
 *
 *  Generator function to handle the TIPS_REQUEST action
 *  Calls the appropriate action, given the api call response
 *
 *  @param {string} token
 *  @param {string} userId
 */
function* requestTips(token, userId) {
  try {
    const response = yield call(api.requestTips, token, userId);

    if (response.error) {
      yield put(actions.tipsFailure(response.error));
    } else {
      const { data } = response.data;
      const tips = allTips(data);

      yield put(actions.tipsSuccess(tips));
    }
  } catch (error) {
    yield put(actions.tipsFailure(error));
  }
}

/**
 *  requestVoteTip
 *
 *  Generator function to handle the TIP_VOTE_REQUEST action
 *  Calls the appropriate action, given the api call response
 *
 *  @param {string} token
 *  @param {string} userId
 *  @param {object} tipId
 */
function* requestVoteTip(token, userId, tipId, vote) {
  try {
    const response = yield call(api.requestVoteTip, token, userId, tipId, vote);

    if (response.error) {
      yield put(actions.voteTipFailure(response.error));
    } else {
      const { data } = response.data;

      yield put(actions.voteTipSuccess(data.id, data.vote));
    }
  } catch (error) {
    yield put(actions.voteTipFailure(error));
  }
}

/**
 *  requestDismissTip
 *
 *  Generator function to handle the TIP_DISMISS_REQUEST action
 *  Calls the appropriate action, given the api call response
 *
 *  @param {string} token
 *  @param {string} userId
 *  @param {object} tipId
 */
function* requestDismissTip(token, userId, tipId) {
  try {
    const response = yield call(api.requestDismissTip, token, userId, tipId);

    if (response.error) {
      yield put(actions.dismissTipFailure(response.error));
    } else {
      const { data } = response.data;

      yield put(actions.dismissTipSuccess(data.id, data.dismissed));
    }
  } catch (error) {
    yield put(actions.dismissTipFailure(error));
  }
}

/**
 *  requestPopularTips
 *
 *  Generator function to handle the POPULAR_TIPS_REQUEST action
 *  Calls the appropriate action, given the api call response
 *
 *  @param {string} token
 *  @param {string} userId
 *  @param {object} tipId
 */
function* requestPopularTips(token) {
  try {
    const response = yield call(api.requestPopularTips, token);

    if (response.error) {
      yield put(actions.popularTipsFailure(response.error));
    } else {
      const { data } = response.data;
      const tips = popularTips(data);

      yield put(actions.popularTipsSuccess(tips));
    }
  } catch (error) {
    yield put(actions.popularTipsFailure(error));
  }
}

/**
 *  Generator function to listen for redux actions
 */
function* watch() {
  while (true) {
    const { type, payload = {} } = yield take([
      TIPS_REQUEST,
      TIP_VOTE_REQUEST,
      TIP_DISMISS_REQUEST,
      POPULAR_TIPS_REQUEST,
    ]);

    switch (type) {
      case TIPS_REQUEST:
        yield fork(requestTips, payload.token, payload.userId);
        break;
      case TIP_VOTE_REQUEST:
        yield fork(requestVoteTip, payload.token, payload.userId, payload.tipId, payload.vote);
        break;
      case TIP_DISMISS_REQUEST:
        yield fork(requestDismissTip, payload.token, payload.userId, payload.tipId);
        break;
      case POPULAR_TIPS_REQUEST:
        yield fork(requestPopularTips, payload.token);
        break;
      default:
        yield null;
    }
  }
}

export default function* rootSaga() {
  yield watch();
}
