import PropTypes from 'prop-types';

export default {
  gridClasses: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.object,
  isMobileSize: PropTypes.bool,
  tips: PropTypes.arrayOf(PropTypes.object),
  factors: PropTypes.arrayOf(PropTypes.object),
  selectTipsFactorFilter: PropTypes.func,
};
