import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { getHistory } from 'modules/router/selectors';
import { getAuthToken } from 'modules/auth/selectors';
import { getUserId } from 'modules/user/selectors';
import { getIsMobileSize } from 'modules/ui/selectors';

import {
  tipsRequest,
  popularTipsRequest,
  dismissTipRequest,
  voteTipRequest,
  selectTipsFactorFilter,
} from '../../actions';

import {
  getTipsWithFactorNames,
  getPopularTipsWithFactorNames,
  getApplicableFactors,
  getSelectedTipsFactor,
} from '../../selectors';

// will need to pull this action in from the transactions module when it exists
import {
  setTransactionsFilterCode,
} from '../../../../actions/creators';

import presenter from './presenter';

const mapStateToProps = createStructuredSelector({
  tips: getTipsWithFactorNames,
  popular: getPopularTipsWithFactorNames,
  factors: getApplicableFactors,
  history: getHistory,
  isMobileSize: getIsMobileSize,
  sessionToken: getAuthToken,
  userId: getUserId,
  selectedTipsFactor: getSelectedTipsFactor,
});

const mapDispatchToProps = {
  tipsRequest,
  voteTipRequest,
  popularTipsRequest,
  dismissTipRequest,
  setTransactionsFilterCode,
  selectTipsFactorFilter,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(presenter);
