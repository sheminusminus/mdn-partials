import React from 'react';
import { isEmpty } from 'lodash';

import { TipsFeedItem, NoTips } from './components';

const FeedItemList = (props) => {
  const {
    tips,
    isMobileSize,
    handleTipFactorSelection,
    voteTip,
    dismissTip,
    noTipsMessage,
  } = props;

  if (isEmpty(tips)) return <NoTips message={noTipsMessage} />;

  return tips.map((tip, idx) => (
    <TipsFeedItem
      handleTipFactorSelection={handleTipFactorSelection}
      handleVote={voteTip}
      handleDismiss={dismissTip}
      isMobileSize={isMobileSize}
      tip={tip}
      key={`tips-feed-item-${idx}`}/>
  ));
};

export default FeedItemList;
