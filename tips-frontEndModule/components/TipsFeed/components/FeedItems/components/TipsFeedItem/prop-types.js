import PropTypes from 'prop-types';

export default {
  gridClasses: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.object,
  isMobileSize: PropTypes.bool,
  tip: PropTypes.object,
  handleDismiss: PropTypes.func,
  handleVote: PropTypes.func,
  handleTipFactorSelection: PropTypes.func,
};
