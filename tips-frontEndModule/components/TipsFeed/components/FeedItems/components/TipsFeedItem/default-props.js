export default {
  gridClasses: 'small-12',
  className: '',
  style: {},
  isMobileSize: false,
  tip: {},
  handleDismiss: () => {},
  handleVote: () => {},
  handleTipFactorSelection: () => {},
};
