import React from 'react';

import { classie, prettyCategoryName } from 'ui/utils';
import { FactorIcons, ContentIcons } from 'ui/constants';

import { SvgInject, Button } from 'Components';

import propTypes from './prop-types';
import defaultProps from './default-props';
import styles from './styles.css';

const ItemFactor = ({ factorName, handleFactorSelection }) => (
  <div className={styles.factorName} role="link" onClick={handleFactorSelection}>
    {prettyCategoryName(factorName)}
  </div>
);

const ItemContent = ({ text }) => (
  <div className={styles.content}>
    {text}
  </div>
);

const LinkedContent = ({ link, linkText, image }) => {
  if (!link) return null;
  return (
    <div className={styles.contentTip}>
      <figure className={styles.figure}>
        <img src={image || ContentIcons.DEFAULT_CONTENT_TIP} alt={linkText} />
      </figure>
      <div className={styles.linkContent}>
        <a href={link} className={styles.linkText} target="_blank">{linkText || link}</a>
      </div>
    </div>
  );
};

const ItemActions = (props) => {
  const { tip, handleUpvote, handleDownvote } = props;

  const upvoteToggles = { [styles.active]: tip.vote > 0 };
  const downvoteToggles = { [styles.active]: tip.vote < 0 };
  const upvoteClasses = classie([styles.upvote], upvoteToggles);
  const downvoteClasses = classie([styles.downvote], downvoteToggles);

  return (
    <div className={styles.actions}>
      <span className={styles.timeAgo}>{tip.timeAgo}</span>
      <Button
        className={upvoteClasses}
        label="Upvote"
        onTouchTap={handleUpvote}/>
      <Button
        className={downvoteClasses}
        label="Downvote"
        onTouchTap={handleDownvote}/>
    </div>
  );
};

class TipsFeedItem extends React.Component {
  constructor(props) {
    super(props);
    this.bindCallbacks();
  }

  bindCallbacks() {
    this.handleUpvote = this.handleUpvote.bind(this);
    this.handleDownvote = this.handleDownvote.bind(this);
    this.handleUnvote = this.handleUnvote.bind(this);
    this.handleFactorSelection = this.handleFactorSelection.bind(this);
  }

  handleFactorSelection() {
    const { handleTipFactorSelection, tip } = this.props;
    handleTipFactorSelection(tip.scoreFactorCode);
  }

  handleUpvote() {
    if (this.props.tip.vote === 1) {
      this.handleUnvote();
      return;
    }
    this.props.handleVote(this.props.tip.id, 1);
  }

  handleDownvote() {
    if (this.props.tip.vote === -1) {
      this.handleUnvote();
      return;
    }
    this.props.handleVote(this.props.tip.id, -1);
  }

  handleUnvote() {
    this.props.handleVote(this.props.tip.id, 0);
  }

  renderIcon() {
    const {
      tip,
      isMobileSize,
    } = this.props;

    return (
      <div className={styles.iconWrapper}>
        <SvgInject
          width={isMobileSize ? 40 : 44}
          height={isMobileSize ? 40 : 44}
          path={FactorIcons[tip.scoreFactorCode] || FactorIcons.PLACEHOLDER} />
      </div>
    );
  }

  render() {
    const {
      gridClasses,
      className,
      style,
      tip,
    } = this.props;

    const { timeAgo, dismissed, vote } = tip;

    const toggles = {
      [styles.new]: (tip.vote === 0 && !tip.dismissed),
    };
    const classes = classie([styles.item, gridClasses, className], toggles);
    const icon = this.renderIcon();

    return (
      <div
        className={classes}
        style={style}>
        {icon}
        <div className={styles.bodyWrapper}>
          <ItemFactor
            handleFactorSelection={this.handleFactorSelection}
            factorName={tip.factorName} />
          <ItemContent text={tip.text} />
          <LinkedContent
            link={tip.link}
            linkText={tip.linkText}
            image={tip.image} />
          <ItemActions
            tip={{ timeAgo, vote }}
            handleDownvote={this.handleDownvote}
            handleUpvote={this.handleUpvote} />
        </div>
      </div>
    );
  }
}

TipsFeedItem.propTypes = propTypes;

TipsFeedItem.defaultProps = defaultProps;

export default TipsFeedItem;
