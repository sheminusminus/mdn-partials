import React from 'react';

import styles from './styles.css';

/**
 * NoData
 * Renders in the TipFeed if there are no tips to display.
 * @constructor
 */
export default ({ message }) => (
  <div className={styles.tipFeedNoData}>
    <div className={styles.sadNoTips} />
    <div className={styles.noDataText}>
      <span className={styles.noDataHeading}>No tips</span>
      <span className={styles.noDataMessage}>
        {message}
      </span>
    </div>
  </div>
);
