import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { uniqBy, concat } from 'lodash';

import { classie } from 'ui/utils';

import { AuthRoutes } from 'ui/constants';

import { FactorFilter, HRFactorFilter } from 'Wrappers';

import { FeedItemList } from './components';

import propTypes from './prop-types';
import defaultProps from './default-props';
import styles from './styles.css';

class Tips extends React.Component {
  constructor(props) {
    super(props);
    this.bindCallbacks();
  }

  bindCallbacks() {
    this.dismissTip = this.dismissTip.bind(this);
    this.voteTip = this.voteTip.bind(this);
    this.getVisibleTips = this.getVisibleTips.bind(this);
    this.handleTipFactorSelection = this.handleTipFactorSelection.bind(this);
  }

  componentDidMount() {
    const { popularTipsRequest, sessionToken } = this.props;
    popularTipsRequest(sessionToken);
  }

  dismissTip(tipId) {
    const { sessionToken, userId, dismissTipRequest } = this.props;
    dismissTipRequest(sessionToken, userId, tipId);
  }

  voteTip(tipId, vote) {
    const { sessionToken, userId, voteTipRequest } = this.props;
    voteTipRequest(sessionToken, userId, tipId, vote);
  }

  handleTipFactorSelection(factorCode) {
    const { setTransactionsFilterCode, history } = this.props;

    setTransactionsFilterCode(factorCode);
    history.push(`${AuthRoutes.COMPARE_CATEGORY}?which=${factorCode.toLowerCase()}`);
  }

  getVisibleTips(tipsType) {
    const { popular, tips, selectedTipsFactor } = this.props;

    let visibleTips = [];
    switch (tipsType) {
      case 'liked':
        visibleTips = uniqBy(concat(tips, popular).filter(tip => tip.vote === 1), 'id');
        break;
      case 'popular':
        visibleTips = popular;
        break;
      default:
        visibleTips = tips;
        break;
    }

    if (selectedTipsFactor && selectedTipsFactor !== 'ALL') {
      return visibleTips.filter(tip => tip.scoreFactorCode === selectedTipsFactor);
    }

    return visibleTips;
  }

  static getNoTipsMessage(tipsType) {
    switch (tipsType) {
      case 'liked':
        return 'Like some tips to show them here! 👍';
      default:
        return 'No tips to show here right now. 📭';
    }
  }

  render() {
    const {
      gridClasses,
      className,
      style,
      factors,
      selectedTipsFactor,
      selectTipsFactorFilter,
      isMobileSize,
    } = this.props;

    const classes = classie([styles.tipsLayout, gridClasses, className]);

    const FactorFilterComponent = isMobileSize ? HRFactorFilter : FactorFilter;
    const filterClasses = isMobileSize ? styles.mobileFilter : '';

    return (
      <div
        className={classes}
        style={style}>
        <div className={styles.sortHeader}>
          <FactorFilterComponent
            className={filterClasses}
            contentClassName={styles.mobileFilterContent}
            onSelect={selectTipsFactorFilter}
            selectedFactorCode={selectedTipsFactor}
            factors={factors} />
        </div>

        <Switch>
          <Route path={AuthRoutes.TIPS_TOP} render={() => (
            <FeedItemList
              isMobileSize={isMobileSize}
              dismissTip={this.dismissTip}
              voteTip={this.voteTip}
              handleTipFactorSelection={this.handleTipFactorSelection}
              noTipsMessage={Tips.getNoTipsMessage()}
              tips={this.getVisibleTips('popular')} />
            )}/>
          <Route path={AuthRoutes.TIPS_LIKED} render={() => (
            <FeedItemList
              isMobileSize={isMobileSize}
              dismissTip={this.dismissTip}
              voteTip={this.voteTip}
              handleTipFactorSelection={this.handleTipFactorSelection}
              noTipsMessage={Tips.getNoTipsMessage('liked')}
              tips={this.getVisibleTips('liked')} />
          )} />
          <Route path="/" render={() => (
            <FeedItemList
              isMobileSize={isMobileSize}
              dismissTip={this.dismissTip}
              voteTip={this.voteTip}
              handleTipFactorSelection={this.handleTipFactorSelection}
              noTipsMessage={Tips.getNoTipsMessage()}
              tips={this.getVisibleTips()} />
          )} />
        </Switch>
      </div>
    );
  }
}

Tips.propTypes = propTypes;

Tips.defaultProps = defaultProps;

export default Tips;
