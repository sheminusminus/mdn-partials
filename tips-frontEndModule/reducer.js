import {
  TIPS_REQUEST,
  TIPS_SUCCESS,
  TIPS_FAILURE,
  TIP_DISMISS_SUCCESS,
  TIP_VOTE_SUCCESS,
  POPULAR_TIPS_REQUEST,
  POPULAR_TIPS_FAILURE,
  POPULAR_TIPS_SUCCESS,
  TIPS_FACTOR_FILTER_SELECTED,
} from './constants';

export const name = 'tipData';

const initialState = {
  tips: [],
  popular: [],
  isFetching: false,
  selectedTipsFactor: 'ALL',
};

export function reducer(state = initialState, action) {
  switch (action.type) {
    case TIPS_REQUEST:
      return {
        ...state,
        isFetching: true,
      };

    case TIPS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        tips: action.payload.tips,
      };

    case TIPS_FAILURE:
      return {
        ...state,
        isFetching: false,
      };

    case TIP_DISMISS_SUCCESS:
      return {
        ...state,
        tips: state.tips.map((tip) => {
          if (tip.id === action.payload.tipId) {
            return {
              ...tip,
              dismissed: action.payload.dismissed,
            };
          }
          return tip;
        }),
        popular: state.popular.map((tip) => {
          if (tip.id === action.payload.tipId) {
            return {
              ...tip,
              dismissed: action.payload.dismissed,
            };
          }
          return tip;
        }),
      };

    case TIP_VOTE_SUCCESS:
      return {
        ...state,
        tips: state.tips.map((tip) => {
          if (tip.id === action.payload.tipId) {
            return {
              ...tip,
              vote: action.payload.vote,
            };
          }
          return tip;
        }),
        popular: state.popular.map((tip) => {
          if (tip.id === action.payload.tipId) {
            return {
              ...tip,
              vote: action.payload.vote,
            };
          }
          return tip;
        }),
      };

    case POPULAR_TIPS_REQUEST:
      return {
        ...state,
        isFetching: true,
      };

    case POPULAR_TIPS_FAILURE:
      return {
        ...state,
        isFetching: false,
      };

    case POPULAR_TIPS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        popular: action.payload.tips,
      };

    case TIPS_FACTOR_FILTER_SELECTED:
      return {
        ...state,
        selectedTipsFactor: action.payload.factorFilter,
      };

    default:
      return state;
  }
}
