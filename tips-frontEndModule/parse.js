import moment from 'moment';
import { sortBy } from 'lodash';

// return a single, formatted tip object
export const singleTip = data => ({
  id: data.id,
  dismissed: data.dismissed,
  vote: data.vote,
  title: data.tip.title,
  text: data.tip.text,
  timeAgo: moment(data.createdAt).fromNow(),
  scoreFactorCode: data.tip.scoreFactorCode,
  image: data.tip.image,
  link: data.tip.link,
  linkText: data.tip.linkText,
});

// return an array of formatted tip objects
export const allTips = data => (
  data.map(tip => singleTip(tip))
);

// sort the popular tips
export const popularTips = (data) => {
  const tips = allTips(data);
  return sortBy(tips, tip => tip.sort);
};
